package com.example.gpstest;

import android.Manifest;
import android.location.Location;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    TextView m_text;
    Button btn_updateLocation;
    GPS m_gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_text = (TextView) findViewById(R.id.text);
        m_gps = new GPS(this);
        btn_updateLocation = findViewById(R.id.btn_updateLocation);
        btn_updateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation();
            }
        });

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                1);
    }

    /**
     * Method that update the text view with the current location on gps.
     */
    void updateLocation()
    {
        Location location = m_gps.getLocation();
        String format = "(" + location.getLatitude() + "," + location.getLongitude() + "," + location.getAltitude() + ")";

        m_text.setText(format);
    }
}
