package com.example.gpstest;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;


public class GPS
{
    Context m_context;
    Location m_location;
    LocationManager m_locationManager;
    String m_provider = LocationManager.GPS_PROVIDER;

    /**
     * Constructor of GPS class check if location is allowed on the device.
     * @param context The context to use
     */
    public GPS(Context context)
    {
        m_context = context;
        m_locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //ask if permission is granted
        if ((ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED))
        {
            //Display error and or quit
            return;
        }

        //Ask updates for the location.
        m_locationManager.requestLocationUpdates(m_provider, 60 * 10, 50, new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) { }

            public void onProviderEnabled(String provider) { }

            public void onProviderDisabled(String provider) { }

            public void onLocationChanged(Location location)
            {
                m_location = location;
            }
        });
    }

    /**
     * Method who give the location for latitude, longitude, altitude as an int
     * @return return the location.
     */
    Location getLocation()
    {
        if(m_location == null)
            if ((ActivityCompat.checkSelfPermission(m_context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) &&
                    (ActivityCompat.checkSelfPermission(m_context, Manifest.permission.ACCESS_COARSE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED))
            {
                m_location = m_locationManager.getLastKnownLocation(m_provider);
            }

            if(m_location == null)
            {
                m_location = new Location(m_provider);
                m_location.setLatitude(0);
                m_location.setLongitude(0);
                m_location.setAltitude(0);
            }

            return m_location;
    }
}
